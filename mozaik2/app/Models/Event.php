<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model{
    protected $fillable = [
        'name',
        'date',
        'location',
        'type',
        'description',
        'picture',
        'visibility',
        'created_by',
    ];

    function scopeFilter($query, array $filters){
        if($filters['search'] ?? false){
            $query->where('name', 'like', '%' .request('search'). '%')
                ->orWhere('date', 'like', '%' .request('search'). '%')
                ->orWhere('location', 'like', '%' .request('search'). '%')
                ->orWhere('type', 'like', '%' .request('search'). '%')
                ->orWhere('description', 'like', '%' .request('search'). '%')

            ;
        }
    }


    public function user(){
        return $this->belongsTo(User::class,'created_by');
    }


    use HasFactory;


}
