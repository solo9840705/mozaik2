<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\View\View;
use function PHPUnit\Framework\logicalNot;

class UserController extends Controller
{
    function canUserView(Event $event): bool
    {
        if (DB::table('visibilities')
            ->where('eventID',$event['id'])
            ->where('userID',auth()->id())
            ->exists()
        ){
            return true;
        }else{
            return false;
        }
    }
    function calculateEvents() : array{

        $alleventsfiltered = Event::latest()->filter(request(['search']))->get();
        $alldisplayableevents = [];
        foreach ($alleventsfiltered as $event){
            if (Auth::check()) {
                if ($event['visibility'] == 'permission') {
                    if ($this->canUserView($event)) {
                        $alldisplayableevents[] = $event;
                    }
                }else{
                    $alldisplayableevents[] = $event;
                }
            }else{
                $alldisplayableevents = Event::latest()->filter(request(['search']))->where('visibility', "true")->get();
            }
        }
        return $alldisplayableevents;
    }

    /**
     * goes to register form
     * @param Request $req
     * @return array|string
     */
    function register(Request $req): array|string
    {

        if (Auth::check()){
            return redirect()->action([EventController::class, 'index']);
        }

        if ($req->ajax()){
            return view('register')->renderSections();
        }

        return view('register');
    }

    /**
     * registers and logs in a user and redirects to events page if form is correctly filled
     * @param Request $req
     * @return RedirectResponse|array
     */
    function userRegister(Request $req): array|RedirectResponse
    {

        if (Auth::check()){
            return redirect()->action([EventController::class, 'index']);
        }

        $formFields = $req->validate([
            'name' => ['required', 'between:3,40'],
            'email' => ['required', 'email', Rule::unique('users','email')],
            'password' => ['required','confirmed','min:6']
        ],
            [
                'name.required'=>'Kötelező megadni nevet!',
                'name.between' => "3 és 40 karakter között kell lennie a névnek",
                'email.required'=>'Kötelező megadni email-t!',
                'email.email'=>'Kötelező az email cím formátum!',
                'email.unique'=>'Az email cím már foglalt!',
                'password.required'=>'Kötelező megadni jelszót!',
                'password.confirmed'=>'A jelszavaknak egyezni kell!',
                'password.min'=>'A jelszónak legalább 6 karakter hósszúnak kell lennie!'
            ]
        );
        //hash pw
        $formFields['password'] = bcrypt($formFields['password']);
        $user = User::create($formFields);

        //login
        auth()->login($user);

        return [view('events', ['events'=>$this->calculateEvents()])->renderSections() ,view('navbar')->renderSections()];
    }

    /**
     * goes to log-in form
     * @return array|string
     */
    function login(Request $req): array|string
    {

        if (Auth::check()){
            return redirect()->action([EventController::class, 'index']);
        }

        if ($req->ajax()){
            return view('login')->renderSections();
        }

        return view('login');
    }

    /**
     * logs in a user and redirects to events page if form is filled correctly
     * @param Request $req
     * @return JsonResponse|array|RedirectResponse
     */
    function userLogin(Request $req): JsonResponse|array|RedirectResponse
    {

        if (Auth::check()){
            return redirect()->action([EventController::class, 'index']);
        }

        $formFields = $req->validate([
            'email' => ['required', 'email'],
            'password' => ['required']
        ],
            [
                'email.required'=>'Kötelező megadni email-t!',
                'email.email'=>'Kötelező az email cím formátum!',
                'password.required'=>'Kötelező megadni jelszót!'
            ]);
        $responsecode = 200;
        if(auth()->attempt($formFields)){
            $req->session()->regenerate();
            return [view('events', ['events'=>$this->calculateEvents()])->renderSections() ,view('navbar')->renderSections()];
        }else{
            $responsecode = 400;
        }
        return response()->json($responsecode);
    }

    /**
     * logs out the currently logged-in user and redirects to events page
     * @param Request $req
     * @return array|View
     */
    function logout(Request $req): array|View
    {

        auth()->logout();

        return [view('events', ['events'=>$this->calculateEvents()])->renderSections() ,view('navbar')->renderSections()];
    }
}
