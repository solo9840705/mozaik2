<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Models\Event;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ApplicationController extends Controller
{
    /**
     * submits an application for the currently logged-in user for an event then redirects to the event's page
     * @param Event $event
     * @return RedirectResponse
     */
    function apply(Event $event): RedirectResponse
    {

        if (!Auth::check()){
            return redirect()->action([EventController::class, 'index']);
        }
        $userid = auth()->id();


        Application::create(['userID' => $userid, 'eventID' => $event['id']]);

        return redirect()->action([EventController::class, 'find'], ['id' => $event['id']]);
    }

    /**
     * removes an application for the currently logged-in user for an event then redirects to the event's page
     * @param Event $event
     * @return RedirectResponse
     */
    function unapply(Event $event): RedirectResponse
    {

        if (!Auth::check()){
            return redirect()->action([EventController::class, 'index']);
        }

        $userid = auth()->id();

        $applicationid = DB::table('applications')->where('userID',$userid)->where('eventID',$event['id'])->value('id');

        Application::destroy($applicationid);

        return redirect()->action([EventController::class, 'find'], ['id' => $event['id']]);
    }
}
