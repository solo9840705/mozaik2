<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Models\Event;
use App\Models\Visibility;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class EventController extends Controller
{
    function calculateEvents(){

        $alleventsfiltered = Event::latest()->filter(request(['search']))->get();
        $alldisplayableevents = [];
        foreach ($alleventsfiltered as $event){
            if (Auth::check()) {
                if ($event['visibility'] == 'permission') {
                    if ($this->canUserView($event)) {
                        $alldisplayableevents[] = $event;
                    }
                }else{
                    $alldisplayableevents[] = $event;
                }
            }else{
                $alldisplayableevents = Event::latest()->filter(request(['search']))->where('visibility', "true")->get();
            }
        }
        return $alldisplayableevents;
    }

    /**
     * lists a single event
     * @param $id
     * @param Request $req
     * @return array|string|View
     */
    function find($id, Request $req): array|string|View
    {
        $userid = auth()->id();
        $filtered = DB::table('applications')->where('userID',$userid)->where('eventID',$id)->value('id');

        if ($filtered !== null){
            $appliedalready = true;
        }else{
            $appliedalready = false;
        }

        $appliedCount = DB::table('applications')->where('eventID',$id)->count();

        if ($req->ajax()){
            return view('event',[
                'event'=> Event::find($id),
                'appliedalready'=> $appliedalready,
                'appliedcount' => $appliedCount
            ])->renderSections();
        }

        return view('event',[
            'event'=> Event::find($id),
            'appliedalready'=> $appliedalready,
            'appliedcount' => $appliedCount
        ]);

    }

    /**
     * checks if currently logged-in user has permission to view the event given in parameter
     * @param Event $event
     * @return bool
     */
    function canUserView(Event $event): bool
    {
        if (DB::table('visibilities')
            ->where('eventID',$event['id'])
            ->where('userID',auth()->id())
            ->exists()
        ){
            return true;
        }else{
            return false;
        }

    }

    /**
     * lists all events the currently logged-in user is permitted to see, filters them if search form is used
     * @param Request $req
     * @return array|string|View
     */
    function index(Request $req): array|string|View
    {

        $alleventsfiltered = Event::latest()->filter(request(['search']))->get();
        $alldisplayableevents = [];
        foreach ($alleventsfiltered as $event){
            if (Auth::check()) {
                if ($event['visibility'] == 'permission') {
                    if ($this->canUserView($event)) {
                        $alldisplayableevents[] = $event;
                    }
                }else{
                    $alldisplayableevents[] = $event;
                }
            }else{
                $alldisplayableevents = Event::latest()->filter(request(['search']))->where('visibility', "true")->get();
            }
        }

        //dd($alleventsfiltered);

        if ($req->ajax()){
            return view('events', [
                'events' => $alldisplayableevents,
                'eventsall' => Event::latest()->filter(request(['search']))->get(),
                'eventsfiltered' => Event::latest()->filter(request(['search']))->where('visibility', "true")->get()])->renderSections();
        }else{
            return view('events', [
                'events' => $alldisplayableevents,
                'eventsall' => Event::latest()->filter(request(['search']))->get(),
                'eventsfiltered' => Event::latest()->filter(request(['search']))->where('visibility', "true")->get()]);
        }

    }

    /**
     * lists the currently logged-in user's own events
     * @param Request $req
     * @return array|string|View
     */
    function ownEvents(Request $req): array|string|View
    {

        if (!Auth::check()){
            return redirect()->action([EventController::class, 'index']);
        }

        $events = Event::latest()->where('created_by',auth()->id())->get();

        if ($req->ajax()){
            return view('ownevents',[
                'events' => $events
            ])->renderSections();
        }

        return view('ownevents',[
           'events' => $events
        ]);
    }

    /**
     * goes to create event form
     * @param Request $req
     * @return array|string|View
     */
    function create(Request $req): array|string|View
    {

        if (!Auth::check()){
            return redirect()->action([EventController::class, 'index']);
        }

        if ($req->ajax()){
            return view('create')->renderSections();
        }

        return view('create');
    }

    /**
     * creates an event and redirects to events page if form is correctly filled
     * @param Request $req
     * @return RedirectResponse
     */
    function createEvent(Request $req): RedirectResponse
    {

        if (!Auth::check()){
            return redirect()->action([EventController::class, 'index']);
        }

        $formfields = $req->validate([
            'name'=> 'required',
            'date'=> 'required',
            'location'=> 'required',
            'type'=> 'required',
            'description'=>'required',
            'visibility'=>'required',
        ],
            [
                'name.required'=>'Kötelező nevet adni az eseménynek!',
                'date.required'=>'Kötelező megadni az esemény idejét!',
                'location.required'=>'Kötelező megadni az esemény helyszínét!',
                'type.required'=>'Kötelező megadni az esemény típusát!',
                'description.required'=>'Kötelező megadni a leírást!',
                'visibility.required'=>'Kötelező megadni a láthatóságot!',
            ]);
        if ($req->hasFile('picture')){
            $file = $req->file('picture');
            $extension = $file->getClientOriginalExtension();
            $filename =  'pictures/' . time() .'.'. $extension;

            $file->move('storage/pictures/', $filename);
            $formfields['picture'] = $filename;
        }else{
            $formfields['picture'] = null;
        }

        $formfields['created_by'] = auth()->id();

        $event = Event::create($formfields);
        Visibility::create(['userID' => auth()->id(), 'eventID'=> $event['id']]);

        return redirect()->action([EventController::class, 'index']);
    }

    /**
     * goes to the editing form of the event given in the parameters
     * @param Event $event
     * @return RedirectResponse|View|array
     */
    function edit(Event $event, Request $req): array|View|RedirectResponse
    {

        if (!Auth::check()){
            return redirect()->action([EventController::class, 'index']);
        }

        if ($req->ajax()){
            return view('edit',['event' => $event])->renderSections();
        }
        return view('edit',['event' => $event]);
    }

    /**
     * updates an event and redirects to the edited event's'page if form is correctly filled
     * @param Request $req
     * @param Event $event
     * @return RedirectResponse|JsonResponse
     */
    function editevent(Request $req, Event $event): RedirectResponse|JsonResponse
    {

        if (!Auth::check()){
            return redirect()->action([EventController::class, 'index']);
        }

        $formfields = $req->validate([
            'name'=> 'required',
            'date'=> 'required',
            'location'=> 'required',
            'type'=> 'required',
            'description'=>'required',
            'visibility'=>'required',
        ],
            [
                'name.required'=>'Kötelező nevet adni az eseménynek!',
                'date.required'=>'Kötelező megadni az esemény idejét!',
                'location.required'=>'Kötelező megadni az esemény helyszínét!',
                'type.required'=>'Kötelező megadni az esemény típusát!',
                'description.required'=>'Kötelező megadni a leírást!',
                'visibility.required'=>'Kötelező megadni a láthatóságot!',
            ]);
        if ($req->hasFile('picture')){
            if ($event['picture'] !== null){
                Storage::disk('public')->delete($event['picture']);
            }

            $file = $req->file('picture');
            $extension = $file->getClientOriginalExtension();
            $filename =  'pictures/' . time() .'.'. $extension;

            $file->move('storage/pictures/', $filename);
            $formfields['picture'] = $filename;


        }

        if ($event->update($formfields)){

        }else{
            dd($event);
        }

        return response()->json([]);
    }

    /**
     * deletes an event given in parameter along with all other instances in DB redirects to events page if succesful
     * @param Event $event
     * @param Request $req
     * @return array|string|View|RedirectResponse
     */
    function destroy(Event $event, Request $req): array|string|View|RedirectResponse
    {

        if (!Auth::check()){
            return redirect()->action([EventController::class, 'index']);
        }

        if ($event['picture'] !== null){
            $image_path = public_path().'/storage/'.$event['picture'];
            unlink($image_path);
            //Storage::disk('public')->delete("/storage/".$event['picture']);
        }

        //$applicationid = DB::table('applications')->where('eventID',$event['id'])->value('id');
        //Application::destroy($applicationid);

        Application::where('eventID',$event['id'])
            ->delete();

        //$visibilityid = DB::table('visibilities')->where('eventID',$event['id'])->value('id');
        //Visibility::destroy($visibilityid);

        Visibility::where('eventID',$event['id'])
            ->delete();

        $event->delete();

        return $this->index($req);

    }
}
