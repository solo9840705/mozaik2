<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Visibility;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class VisibilityController extends Controller
{
    /**
     * goes to an event's grant visibility page
     * @param Event $event
     * @return array|string
     */
    function grantVisibility(Event $event, Request $req): array|string
    {

        if (!Auth::check()){
            return redirect()->action([EventController::class, 'index']);
        }

        //gets all user ids added to an event
        $addedUsersID = DB::table('visibilities')->where('eventID',$event['id'])->pluck('userID');
        //gets all users not added to an event
        $notAddedUsers = DB::table('users')->whereNotIn('id',$addedUsersID)->get();
        //gets all users added to an event
        $addedUsers =  DB::table('users')->whereIn('id',$addedUsersID)->whereNot('id',auth()->id())->get();

        if ($req->ajax()){
            return view('grantvisibility', [
                'notaddedusers' => $notAddedUsers,
                'addedusers' => $addedUsers,
                'event' => $event
            ])->renderSections();
        }

        return view('grantvisibility', [
            'notaddedusers' => $notAddedUsers,
            'addedusers' => $addedUsers,
            'event' => $event
        ]);
    }

    /**
     * grants permission to a user to see an event redirects to grantvisibility page
     * @param Request $req
     * @param Event $event
     * @return RedirectResponse|array
     */
    function addVisibility(Request $req,Event $event): array|string|RedirectResponse
    {

        if (!Auth::check()){
            return redirect()->action([EventController::class, 'index']);
        }

        $formfields = $req->validate(
            [
                'notaddedppl'=> 'required'
            ],
            [
                'notaddedppl.required'=> 'Kötelező valakit kiválasztani!'
            ]
        );
        $formfields['userID'] = $formfields['notaddedppl'];
        $formfields['eventID'] = $event['id'];

        Visibility::create($formfields);

        return $this->grantVisibility($event,$req);
    }

    /**
     * removes permission from a user to see an event redirects to grantvisibility page
     * @param Request $req
     * @param Event $event
     * @return RedirectResponse
     */
    function removeVisibility(Request $req,Event $event): RedirectResponse
    {

        if (!Auth::check()){
            return redirect()->action([EventController::class, 'index']);
        }

        $formfields = $req->validate(
            [
                'addedppl'=> 'required'
            ],
            [
                'addedppl.required'=> 'Kötelező valakit kiválasztani!'
            ]
        );
        $formfields['userID'] = $formfields['addedppl'];
        $formfields['eventID'] = $event['id'];

        $visibilityid = DB::table('visibilities')
            ->where('userID',$formfields['userID'])
            ->where('eventID',$formfields['eventID'])
            ->value('id');

        Visibility::destroy($visibilityid);

        return redirect()->action([VisibilityController::class, 'grantVisibility'], ['event' => $event]);
    }
}
