<?php

use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VisibilityController;
use App\Models\Event;
use http\Env\Request;
use Illuminate\Support\Facades\Route;

//lists all event
Route::get('/', [EventController::class, 'index'] );
Route::get('/events', [EventController::class, 'index'] );

//goes to log-in form
Route::get('/login', [UserController::class, 'login'] );

//goes to register form
Route::get('/register', [UserController::class, 'register'] );

//logs out the currently logged-in user
Route::get('/logout', [UserController::class, 'logout'] );

//goes to create event form
Route::get('/create', [EventController::class, 'create'] );

//lists the currently logged-in user's own events
Route::get('/ownevents', [EventController::class, 'ownEvents'] );

//goes to event edit form
Route::get('/{event}/edit', [EventController::class, 'edit'] );

//goes to an event's visibility management page
Route::get('/{event}/grantvisibility', [VisibilityController::class, 'grantVisibility'] );

//processes a submited register form
Route::post('/register', [UserController::class, 'userRegister'] );

//processes a submited log-in form
Route::post('/login', [UserController::class, 'userLogin'] );

//processes a submited create event form
Route::post('/createevent', [EventController::class, 'createEvent'] );

//processes a submited grant visibility form
Route::post('/{event}/grantvisibility', [VisibilityController::class, 'addVisibility'] );

//processes submited remove visibility form
Route::delete('/{event}/removevisibility', [VisibilityController::class, 'removeVisibility'] );

//processes a submited update event form
Route::put('/{event}', [EventController::class, 'editevent']);

//processes a submitted delete event
Route::delete('/{event}' ,[EventController::class, 'destroy']);

//makes an application of the currently logged-in user to an event
Route::get('/{event}/apply', [ApplicationController::class, 'apply'] );

//deletes an application of the currently logged-in user to an event
Route::get('/{event}/unapply', [ApplicationController::class, 'unapply'] );

//goes to an event
Route::get('/{id}', [EventController::class, 'find'] );


