<?php

namespace Database\Seeders;

use App\Models\Event;
use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Visibility;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {

        User::create([
        'name' => 'Balogh Gellért',
        'email' => 'baloghgellert0424@gmail.com',
        'password' => '$2y$12$mDYulUVkECISrwfTjsrvKuiX.atIAJcfZwtZVkwn2e8uH5t8wxX4O' //asd123
        ]);
        User::create([
            'name' => 'Másik felhasználó',
            'email' => 'teszt2@gmail.com',
            'password' => '$2y$12$mDYulUVkECISrwfTjsrvKuiX.atIAJcfZwtZVkwn2e8uH5t8wxX4O' //asd123
        ]);

        Event::create([
            'name' => 'Társasozás a Gamerlandben',
            'date' => '2024-06-20 16:00',
            'location' => 'Szeged Gamerland',
            'type' => 'társasjáték',
            'description' => 'Társasozás a Gamerlanden kezdőknek és haladóknak egyaránt.',
            'visibility' => 'true',
            'picture' => 'pictures/1718643158.jpg',
            'created_by' => '1'
        ]);
        Event::create([
            'name' => 'Közös filmezés a moziban',
            'date' => '2024-06-25 19:00',
            'location' => 'Szeged CinemaCity',
            'type' => 'film',
            'description' => 'Majmok Bolygója film közös megnézése a moziban és utána kitárgyalása.',
            'visibility' => 'false',
            'picture' => 'pictures/1718644779.png',
            'created_by' => '1'
        ]);
        Event::create([
            'name' => 'Közös fagyizás',
            'date' => '2024-06-30 13:00',
            'location' => 'Szeged Levendula Kézműves Fagylaltozó',
            'type' => 'étel',
            'description' => 'Közös fagyizás Szegeden a Levendula Kézműves fagyizóban',
            'visibility' => 'permission',
            'picture' => 'pictures/1718647343.jpg',
            'created_by' => '2'
        ]);
        Visibility::create([
            'eventID' => '3',
            'userID' => '2',
        ]);


        // User::factory(10)->create();
        /*
        User::factory()->create([
            'name' => 'Test User',
            'email' => 'test@example.com',
        ]);
        */
    }
}
