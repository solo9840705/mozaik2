$(document).ready(function () {


    $('a').click(function (event) {
        if (this.href !== "http://127.0.0.1:8000/logout") {
            event.preventDefault()
            window.history.pushState({}, "", this.href);

            jQuery.ajax({
                url: this.href,
                type: 'get',

                success: function (result) {
                    $('#testid').html(result['content'])
                }
            })
        }else {
            event.preventDefault()

            jQuery.ajax({
                url:'/logout',
                type:'get',

                success:function (result){
                    window.history.pushState({}, "", "/events");
                    $('#testid').html(result[0]['content'])
                    $('#nav').html(result[1]['nav'])
                },

            })
        }
    })

    $('#searchevents').on('submit', function (event){
        event.preventDefault()
        window.history.pushState({}, "", "/events");

        jQuery.ajax({
            url:"/events",
            data:jQuery('#searchevents').serialize(),
            type:'get',

            success:function (result){

                $('#testid').html(result['content'])
            }
        })

    })


})
