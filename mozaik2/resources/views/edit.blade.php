@extends('layout')

@section('content')
    <head>
        <title>Esemény szerkesztése</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script>
            $('#editform').on('submit',function (event){
                event.preventDefault()
                let event_id = $('#editbutton').val()

                jQuery.ajax({
                    url:'/'+event_id,
                    data:new FormData(this),
                    type:'post',
                    contentType:false,
                    processData:false,

                    success:function (result){
                        $('.errorbox').text("")
                        $('.editsuccess').text("Esemény sikeresen szerkesztve")
                    },

                    error:function (errors){
                        let errorsimplified = errors['responseJSON']['errors']
                        $('.editsuccess').text("")
                        $('.errorbox').text("")
                        for (let key in errorsimplified){
                            $('.'+key).text(errorsimplified[key][0])
                        }
                    }
                })
            })
            $('.backbutton').click(function (event){
                event.preventDefault()
                window.history.pushState({}, "", this.href);

                jQuery.ajax({
                    url:this.href,
                    type:'get',

                    success:function(result){

                        $('#testid').html(result['content'])
                    }
                })
            })
        </script>
    </head>
    <div class="loginbody">
        <div class="wrapper">
            <div class="editsuccess"></div>
            <form action="/{{$event->id}}" method="post" enctype="multipart/form-data" class="update-form" id="editform">
                @csrf
                @method('put')

                <a href="/{{$event['id']}}>" class="backbutton">
                    <i class="fa fa-caret-left"></i>
                </a>

                <div class="input-box">
                    <input type="text" name="name" placeholder="Az esemény elnevezése" value="{{$event->name}}">
                </div>

                <p class="name errorbox"></p>

                <div class="input-box">
                    <input type="text" name="date" placeholder="Az esemény időpontja" value="{{$event->date}}">
                </div>

                <p class="date errorbox"></p>

                <div class="input-box">
                    <input type="text" name="location" placeholder="Az esemény helyszíne" value="{{$event->location}}">
                </div>

                <p class="location errorbox"></p>

                <div class="input-box">
                    <input type="text"  name="type" placeholder="Az esemény típusa" value="{{$event->type}}">
                </div>

                <p class="type errorbox"></p>

                <div class="textarea-box">
                    <textarea name="description" placeholder="Az esemény leírása">{{$event->description}}</textarea>
                </div>

                <p class="description errorbox"></p>

                <div class="input-box">
                    <label for="visibility">Láthatóság:
                    <select name="visibility">
                        @if($event -> visibility == "true")
                            <option value="true">Bárki</option>
                            <option value="false">Csak regisztrált felhasználók</option>
                            <option value="permission">Csak kiválasztott felhasználók</option>
                        @elseif($event -> visibility == "false")
                            <option value="false">Csak regisztrált felhasználók</option>
                            <option value="true">Bárki</option>
                            <option value="permission">Csak kiválasztott felhasználók</option>
                        @else
                            <option value="permission">Csak kiválasztott felhasználók</option>
                            <option value="false">Csak regisztrált felhasználók</option>
                            <option value="true">Bárki</option>
                        @endif
                    </select>
                    </label>
                </div>

                <div class="input-box">
                    <input type="file" name="picture">
                </div>

                <button type="submit" class="button" id="editbutton" value="{{$event['id']}}">Befejezés</button>
                <br>
            </form>
        </div>
    </div>
@endsection
