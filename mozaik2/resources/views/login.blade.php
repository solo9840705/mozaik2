@extends('layout')
@section('content')
    <head>
        <title>Bejelentkezés</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script>
            $(document).ready(function () {
                $("#loginform").on("submit", function (event) {

                    event.preventDefault()

                    jQuery.ajax({

                        url: "/login",
                        data: jQuery('#loginform').serialize(),
                        type: 'post',

                        success: function (result) {
                            console.log(result)
                            window.history.pushState({}, "", "/events");
                            console.log(result)
                            $('#testid').html(result[0]['content'])
                            $('#nav').html(result[1]['nav'])
                        },

                        error: function (errors){
                            console.log(errors)
                            let errorsimplified = errors['responseJSON']['errors']
                            $('.singleerror').text("Valamelyik adat helytelen")
                            $('.errorbox').text("")
                            for (let key in errorsimplified){
                                $('.'+key).text(errorsimplified[key][0])
                            }
                        }
                    })
                })
            })
        </script>
    </head>
<body>

</body>
    <div class="loginbody">
        <div class="wrapper">
            <form action="/login" method="post" id="loginform" novalidate>
                @csrf
                <p class="singleerror"></p>

                <div class="input-box">
                    <input type="text" name="email" placeholder="Adja meg az email címét!" value="{{old('email')}}">
                </div>

                <p class="email errorbox"></p>

                <div class="input-box">
                    <input type="password" name="password" placeholder="Adja meg a jelszavát!">
                </div>

                <p class="password errorbox"></p>

                <button type="submit" class="button">Bejelentkezés</button>
                <br>

            </form>
        </div>
    </div>

@endsection
