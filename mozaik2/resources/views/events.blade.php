@extends('layout')
@section('content')
    <head>
        <title>Események</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script>
            $(document).ready(function (){
                $('.eventanchor').click(function (event){
                    event.preventDefault()
                    window.history.pushState({}, "", this.href);

                    jQuery.ajax({
                        url:this.href,
                        type:'get',

                        success:function(result){

                            $('#testid').html(result['content'])
                        }
                    })
                })
            })
        </script>
    </head>
    <div class="eventcontainer">
        @foreach($events as $event)
            <div class="event">
                <div class="picdiv">
                    <div>
                        <img class="pic" src="{{$event->picture ? asset('storage/'.$event->picture) : asset('storage/pictures/default.png')}}" alt="">
                    </div>
                </div>
                <div class="lineheight100px">
                    <h2><a class="eventanchor" href="/{{$event['id']}}">{{$event['name']}}</a></h2>
                </div>
                <div class="lineheight100px colorwhite">
                    {{$event['location']}}
                </div>
                <div class="lineheight100px colorlightsalmon">
                    {{$event['type']}}
                </div>
            </div>
        @endforeach

    </div>
@endsection
