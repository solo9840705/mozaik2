@extends('layout')
@section('content')
    <head>
        <title>Felhasználók hozzárendelése</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script>
            $(document).ready(function (){

                $('#grantvisibilityform').on('submit',function (event){

                    event.preventDefault()
                    let event_id = $('#grantvisibilitybutton').val()


                    jQuery.ajax({
                        url:'/'+event_id+'/grantvisibility',
                        type:'post',
                        data:new FormData(this),
                        contentType:false,
                        processData:false,

                        success:function (result){
                            let url = "/"+event_id+"/grantvisibility"
                            window.history.pushState({}, "", url);
                            $('#testid').html(result['content'])
                        },

                        error:function (errors){
                        }
                    })
                })

                $('#removevisibilityform').on('submit',function (event){

                    event.preventDefault()
                    let event_id = $('#removevisibilitybutton').val()


                    jQuery.ajax({
                        url:'/'+event_id+'/removevisibility',
                        type:'post',
                        data:new FormData(this),
                        contentType:false,
                        processData:false,

                        success:function (result){
                            let url = "/"+event_id+"/grantvisibility"
                            window.history.pushState({}, "", url);
                            $('#testid').html(result['content'])
                        },

                        error:function (errors){
                        }
                    })
                })
                $('.backbutton').click(function (event){
                    event.preventDefault()
                    window.history.pushState({}, "", this.href);

                    jQuery.ajax({
                        url:this.href,
                        type:'get',

                        success:function(result){

                            $('#testid').html(result['content'])
                        }
                    })
                })
            })

        </script>
    </head>
    <div class="loginbody">
        <div class="wrapper">

            <a href="/{{$event['id']}}>" class="backbutton">
                <i class="fa fa-caret-left"></i>
            </a>

            <form action="/{{$event->id}}/grantvisibility" method="post" enctype="multipart/form-data" id="grantvisibilityform">
                @csrf
                <div class="input-box">
                    <label for="addedppl">Nem hozzáadott felhasználók:
                    <select name="notaddedppl">
                        @foreach($notaddedusers as $user)
                            <option value="{{$user->id}}">{{$user->name}}</option>
                        @endforeach
                    </select>
                    </label>
                </div>

                <button type="submit" class="button" id="grantvisibilitybutton" value="{{$event['id']}}">Hozzáadás</button>

            </form>
            <form action="/{{$event->id}}/removevisibility" method="post" enctype="multipart/form-data" id="removevisibilityform">
                @csrf
                @method('delete')
                <div class="input-box">
                    <label for="addedppl">Hozzáadott felhasználók:
                        <select name="addedppl">
                            @foreach($addedusers as $user)
                                <option value="{{$user->id}}">{{$user->name}}</option>
                            @endforeach
                        </select>
                    </label>
                </div>
                <button type="submit" class="button" id="removevisibilitybutton" value="{{$event['id']}}">Eltávolítás</button>
            </form>
        </div>
    </div>
@endsection
