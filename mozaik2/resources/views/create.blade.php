@extends('layout')
@section('content')
    <head>
        <title>Esemény létrehozása</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script>
            $(document).ready(function (){
                $("#createform").on("submit",function (event){

                    event.preventDefault()

                    jQuery.ajax({

                        url:"/createevent",
                        type:'post',
                        data:new FormData(this),
                        contentType:false,
                        processData:false,


                        success:function (result){

                            $('.errorbox').text("")
                            $('.createsuccess').text("Esemény sikeresen létrehozva")
                            jQuery('#createform')[0].reset()
                            $('html, body').animate({scrollTop: 0}, 600)

                        },

                        error:function (errors){
                            let errorsimplified = errors['responseJSON']['errors']
                            $('.createsuccess').text("")
                            $('.errorbox').text("")
                            for (let key in errorsimplified){
                                $('.'+key).text(errorsimplified[key][0])
                            }
                        }
                    })
                })
            })
        </script>
    </head>
    <div class="loginbody">
        <div class="wrapper">
            <form action="/createevent" method="post" enctype="multipart/form-data" id="createform">
                @csrf

                <div class="createsuccess"></div>

                <div class="input-box">
                    <input type="text" name="name" placeholder="Az esemény elnevezése" value="{{old('name')}}">
                </div>

                <p class="name errorbox"></p>

                <div class="input-box">
                    <input type="text" name="date" placeholder="Az esemény időpontja ÉV-HÓNAP-NAP ÓRA:PERC" value="{{old('date')}}">
                </div>

                <p class="date errorbox"></p>

                <div class="input-box">
                    <input type="text" name="location" placeholder="Az esemény helyszíne" value="{{old('location')}}">
                </div>

                <p class="location errorbox"></p>

                <div class="input-box">
                    <input type="text"  name="type" placeholder="Az esemény típusa" value="{{old('type')}}">
                </div>

                <p class="type errorbox"></p>

                <div class="textarea-box">
                    <textarea name="description" placeholder="Az esemény leírása">{{old('description')}}</textarea>
                </div>

                <p class="description errorbox"></p>

                <div class="input-box">
                    <label for="visibility">Láthatóság:</label>
                    <select name="visibility">
                        <option value="true">Bárki</option>
                        <option value="false">Csak regisztrált felhasználók</option>
                        <option value="permission">Csak kiválasztott felhasználók</option>
                    </select>
                </div>

                <div class="input-box">
                    <input type="file" name="picture">
                </div>

                <p class="cover errorbox"></p>

                <button type="submit" class="button">Létrehozás</button>

            </form>
        </div>
    </div>
@endsection
