@extends('layout')
@section('content')
    <head>
        <title>{{$event->name}}</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script>
            $(document).ready(function (){
                $('.editanchor').click(function (event){
                    event.preventDefault()
                    window.history.pushState({}, "", this.href);

                    jQuery.ajax({
                        url:this.href,
                        type:'get',

                        success:function(result){

                            $('#testid').html(result['content'])
                        }
                    })
                })

                $('#deleteevent').on('submit',function (event){

                    event.preventDefault()
                    let even_id = $('#submitdelete').val()

                    jQuery.ajax({
                        url:'/'+even_id,
                        data:{
                            "event": even_id,
                            "_token": "{{csrf_token()}}"
                        },
                        type:'delete',

                        success:function (result){
                            window.history.pushState({}, "", "/events");
                            $('#testid').html(result['content'])
                        },

                        error:function (errors){
                        }
                    })
                })
            })
        </script>
    </head>
    <div class="singleeventcontainer">
        <div class="singleevent">
            <div class="container">

                <div class="event-image">
                    <img class="eventpicture" src="{{$event->picture ? asset('storage/'.$event->picture) : asset('storage/pictures/default.png')}}" alt="">
                </div>
                <div class="event-details">
                    <h2>{{$event['name']}}</h2>
                    <p class="colorwhite">Időpont: {{$event['date']}}</p>
                    <p class="colorwhite">Helyszín: {{$event['location']}}</p>
                    <p class="colorwhite">Típus: {{$event['type']}}</p>
                    <p class="colorwhite">Jelentkezők: {{$appliedcount}}</p>
                    <div class="line"></div>
                    <p class="colorwhite">Leírás: {{$event['description']}}</p>
                </div>
            </div>
        </div>
    </div>
    @auth()
    @if($event['created_by'] == auth()->user()->id)
    <div class="mainbuttonsdiv ">
        <a href="/{{$event->id}}/edit" class="editanchor">
            <div class="mainbuttons">
                Szerkesztés
            </div>
        </a>
        @if($event['visibility'] == 'permission')
            <br>
        <a href="/{{$event->id}}/grantvisibility" class="editanchor">
            <div class="mainbuttons">
                Felhasználók hozzárendelése
            </div>
        </a>
        @endif
        <form action="/{{$event['id']}}" method="post" id="deleteevent">
            @csrf
            @method('delete')
            <button type="submit" class="mainbuttons" value="{{$event['id']}}" id="submitdelete">
                Törlés
            </button>
        </form>
    @else
            @if(!$appliedalready)
            <div class="mainbuttonsdiv ">
            <a href="/{{$event->id}}/apply" class="editanchor" id="apply">
                <div class="mainbuttons">
                    Jelentkezés
                </div>
            </a>
            </div>
            @else
                <div class="mainbuttonsdiv ">
                    <a href="/{{$event->id}}/unapply" class="editanchor" id="unapply">
                        <div class="mainbuttons">
                            Lejelentkezés
                        </div>
                    </a>
                </div>
            @endif
    </div>
    @endif
    @endauth
@endsection
