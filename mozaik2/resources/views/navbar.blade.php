@extends('layout')

@section('nav')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="{{ asset('/script.js') }}" type="text/javascript"></script>
<ul id="nav">
    @auth
        <li><a href="/events" id="events">Események</a></li>
        <li>
            <form class="navsearch" name="search" action="/events" id="searchevents">
                <div class="search">
                    <button type="submit"><i class="fa fa-search"></i></button>
                    <input type="search" class="search-input" name="search" placeholder="Keresés">
                </div>
            </form>
        </li>
        <li><a href="/create" id="logout">Új esemény kiírása</a></li>
        <li><a href="/ownevents" id="logout">Saját események</a></li>
        <li><a href="/logout" id="logout">Kijelentkezés</a></li>

    @else
        <li><a href="/events" id="events">Események</a></li>
        <li><a href="/login" id="login">Bejelentkezés</a></li>
        <li><a href="/register" id="register">Regisztráció</a></li>
    @endauth
</ul>

@endsection
