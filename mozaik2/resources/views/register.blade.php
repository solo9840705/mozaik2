@extends('layout')
@section('content')
<head>
    <title>Regisztráció</title>
</head>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $(document).ready(function () {
        $("#registerform").on("submit", function (event) {

            event.preventDefault()

            jQuery.ajax({

                url: "/register",
                data: jQuery('#registerform').serialize(),
                type: 'post',

                success: function (result) {
                    window.history.pushState({}, "", "/events");
                    $('#testid').html(result[0]['content'])
                    $('#nav').html(result[1]['nav'])
                },

                error:function (errors){
                    let errorsimplified = errors['responseJSON']['errors']
                    $('.errorbox').text("")
                    for (let key in errorsimplified){
                        $('.'+key).text(errorsimplified[key][0])
                    }
                }
            })
        })
    })
</script>
<div class="loginbody">
    <div class="wrapper">
        <form method="POST" action="/register" id="registerform" novalidate>
            @csrf
            <div class="input-box">
                <input type="text" name="name" placeholder="Adja meg a nevét!" value="{{old('name')}}">
            </div>

            <p class="name errorbox"></p>

            <div class="input-box">
                <input type="email" name="email" placeholder="Adja meg az email címét!" value="{{old('email')}}">
            </div>

            <p class="email errorbox"></p>

            <div class="input-box">
                <input type="password" name="password" placeholder="Adja meg a jelszavát!" value="{{old('password')}}">
            </div>

            <p class="password errorbox"></p>

            <div class="input-box">
                <input type="password" name="password_confirmation" placeholder="Adja meg a jelszavát megint!" value="{{old('password_confirmation')}}">
            </div>

            <p class="password_confirmation errorbox"></p>

            <button type="submit" class="button">Regisztráció</button>
            <br>
        </form>
    </div>
</div>
@endsection
